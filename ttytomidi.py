import serial, time, jack
ser = serial.Serial('/dev/ttyAMA0', 31250)
client = jack.Client('MyGreatClient')
midiout = client.midi_inports.register('midi_out')
client.activate()
#client.connect(midiout, 'ZynMidiRouter:main_in')
print(ser.name)

while 1:
    midiout.clear_buffer()
    serial_line = ser.read(4)
    print(serial_line)
    midiout.write_midi_event( client.frames_since_cycle_start , serial_line)
